

import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import GameMenu from './views/GameMenu'
import GamePlay from './views/GamePlay'
import GameOver from './views/GameOver'

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: 'game-menu',
    component: GameMenu
  },
  {
    path: "/GamePlay",
    name: 'game-play',
    component: GamePlay
  },
  {
    path: "/GameOver",
    name: 'game-over',
    component: GameOver
  }
]

const router = new VueRouter({routes});

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')

